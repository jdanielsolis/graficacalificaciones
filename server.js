require('./core/ini'); //inicializamos las configuraciones
//Inicializamos 
const host = process.env.host;
const port = process.env.port;

//Importamos la librerias necesarias para la ejecución del proyecto
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const hbs = require('express-hbs');
const open = require('open');
const favicon = require('serve-favicon');
const path = require('path')
const useragent = require('express-useragent');
const morgan = require('morgan');
const helmet = require('helmet');
const fs = require('fs-extra');

morgan.token('date', (req, res) => { let date = new Date(); return `[${date.toLocaleDateString()} ${date.toLocaleTimeString()}] - ${req.ip}` })
morgan.format('customeFormat', ':date ":method :url" :status :res[content-length] - :response-time ms');

app.engine('.hbs', hbs.express4());

app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, "/views"));

app.use(helmet());
app.use(express.static(path.join(__dirname, "/public")));
app.use(favicon(path.join(__dirname, "/public/assets/img/favicon.png")));
app.use(morgan('dev', { skip: (req, res) => res.statusCode < 400 }))
app.use(morgan('customeFormat', { stream: fs.createWriteStream(path.join(__dirname, '/logs', 'server.log'), { flags: 'a' }) }))
app.use(useragent.express());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', require('./routes/graph'));

app.listen(port, () => {
  console.log('Servidor en puerto', port);
  open(`http://${host}:${port}`);
});